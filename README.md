# README #

Este repositorio se hace para presentaciones externas, son presentaciones y archivos que requiero en Web.

# Presentaciones

A partir de los desarrollado en los proyectos, se irá actualizando esta parte

## Microeconomía

- [Normas Sociales](https://bb.githack.com/Keynes37/externals/raw/main/Micro/Normas-sociales.html)

## Series de tiempo

- [Presentación Univariado ISE](https://bb.githack.com/Keynes37/externals/raw/main/IMSEA/Reporte-Sarima.html)

